package simulacro.com.eduardorabanal.app.simulacro2.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simulacro.com.eduardorabanal.app.simulacro2.R;
import simulacro.com.eduardorabanal.app.simulacro2.adapters.ContactoAdapterLV;
import simulacro.com.eduardorabanal.app.simulacro2.interfaces.IListActivity;
import simulacro.com.eduardorabanal.app.simulacro2.models.Contacto;
import simulacro.com.eduardorabanal.app.simulacro2.services.ContactoServiceProvider;
import simulacro.com.eduardorabanal.app.simulacro2.services.IContactoService;

public class ContactoListActivity extends AppCompatActivity implements IListActivity {
    private static final String TAG = ContactoServiceProvider.TAG;

    private ListView lv;
    private ContactoAdapterLV adapter;
    private boolean soloFavoritos;
    private IContactoService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto_list);

        soloFavoritos= getIntent().getExtras().getBoolean("soloFavoritos");
        service = ContactoServiceProvider.getService();

        lv=(ListView) findViewById(R.id.lv);
        adapter=new ContactoAdapterLV();
        adapter.setActivity(this);
        adapter.setService(service);
        lv.setAdapter(adapter);
        
        SetListData();
    }

    @Override
    public void SetListData() {
        Call<List<Contacto>> respuestaCall;

        if(soloFavoritos)
            respuestaCall =  service.GetFavoritos();
        else
            respuestaCall =  service.GetContactos();

        respuestaCall.enqueue(new Callback<List<Contacto>>() {
            @Override
            public void onResponse(Call<List<Contacto>> call, Response<List<Contacto>> response) {
                if(response.isSuccessful())
                {
                    List<Contacto> lista = response.body();
                    adapter.setDataSet(lista);
                }
                else
                {
                    Log.e(TAG," onResponse" + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<Contacto>> call, Throwable t) {
                Log.e(TAG,t.getMessage());
            }
        });
    }
}
