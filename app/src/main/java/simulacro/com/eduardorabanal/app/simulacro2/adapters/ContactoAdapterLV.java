package simulacro.com.eduardorabanal.app.simulacro2.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simulacro.com.eduardorabanal.app.simulacro2.R;
import simulacro.com.eduardorabanal.app.simulacro2.interfaces.IListActivity;
import simulacro.com.eduardorabanal.app.simulacro2.models.Contacto;
import simulacro.com.eduardorabanal.app.simulacro2.services.ContactoServiceProvider;
import simulacro.com.eduardorabanal.app.simulacro2.services.IContactoService;

/**
 * Created by USER on 08/07/2017.
 */
public class ContactoAdapterLV extends BaseAdapter {
    private static final String TAG = ContactoServiceProvider.TAG;

    private List<Contacto> dataset;
    private IContactoService service;
    private IListActivity activity;

    public void setActivity(IListActivity activity) {
        this.activity = activity;
    }

    public void setService(IContactoService service) {
        this.service = service;
    }

    public ContactoAdapterLV() {
        dataset= new ArrayList<>();
    }

    @Override
    public int getCount() {
        return dataset.size();
    }

    @Override
    public Contacto getItem(int position) {
        return dataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacto,parent,false);
        }

        final Contacto obj = getItem(position);

        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        Glide.with(parent.getContext())
                .load(obj.getImgUrl())
                .animate(android.R.anim.slide_in_left)
                .into(image);

        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(obj.getNombre());

        TextView phone = (TextView) convertView.findViewById(R.id.phone);
        phone.setText(obj.getTelefono());

        ImageView isFav = (ImageView) convertView.findViewById(R.id.isFav);
        if(obj.isEsFavorito())
        {
            isFav.setImageResource(android.R.drawable.star_big_on);
        }else{
            isFav.setImageResource(android.R.drawable.star_big_off);
        }

        isFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> rptaCall= service.UpdateContacto(obj.getContactoId());
                rptaCall.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful())
                        {
                            Log.i(TAG, response.message());
                            activity.SetListData();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });


            }
        });

        return convertView;
    }

    public void setDataSet(List<Contacto> dataset) {
        this.dataset = dataset;
        notifyDataSetChanged();
    }
}
