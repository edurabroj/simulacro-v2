package simulacro.com.eduardorabanal.app.simulacro2.models;

/**
 * Created by USER on 08/07/2017.
 */
public class Contacto {
    private int ContactoId;
    private String Nombre;
    private String Telefono;
    private String ImgUrl;
    private boolean EsFavorito;

    public int getContactoId() {
        return ContactoId;
    }

    public void setContactoId(int contactoId) {
        ContactoId = contactoId;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        ImgUrl = imgUrl;
    }

    public boolean isEsFavorito() {
        return EsFavorito;
    }

    public void setEsFavorito(boolean esFavorito) {
        EsFavorito = esFavorito;
    }
}
