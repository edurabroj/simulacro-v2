package simulacro.com.eduardorabanal.app.simulacro2.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 08/07/2017.
 */
public class ContactoServiceProvider {
    public static IContactoService getService()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.2/simulacro/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(IContactoService.class);
    }
    public static final String TAG = "CONTACTO";
}
