package simulacro.com.eduardorabanal.app.simulacro2.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import simulacro.com.eduardorabanal.app.simulacro2.R;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button btnTodos = (Button) findViewById(R.id.btnTodos);
        assert btnTodos != null;
        btnTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ContactoListActivity.class);
                i.putExtra("soloFavoritos", false);
                startActivity(i);
            }
        });

        Button btnFavoritos = (Button) findViewById(R.id.btnFavoritos);
        assert btnFavoritos != null;
        btnFavoritos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ContactoListActivity.class);
                i.putExtra("soloFavoritos", true);
                startActivity(i);
            }
        });
    }
}
