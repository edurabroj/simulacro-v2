package simulacro.com.eduardorabanal.app.simulacro2.services;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import simulacro.com.eduardorabanal.app.simulacro2.models.Contacto;

/**
 * Created by USER on 08/07/2017.
 */
public interface IContactoService {
    @GET("home/index")
    Call<List<Contacto>> GetContactos();

    @GET("home/favoritos")
    Call<List<Contacto>> GetFavoritos();

    @FormUrlEncoded
    @POST("home/update")
    Call<ResponseBody> UpdateContacto(@Field("id") int id);
}
